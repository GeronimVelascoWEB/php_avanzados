<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Ruta de prueba

Route::get('hola',function(){
    for($i=1; $i<=20; $i++){

        echo "Jesus maria ".$i."<br>";
    }
});


// Definir ruta para la vista
Route::get("arreglos", function(){

    $estudiantes = ["ma" => "Maria", "p" => "Petro"];
    echo "<pre>";

    //mostramos los datos del arreglos " para errores"
    print_r($estudiantes);
    echo "</pre>";
// Recorremos el arreglo con el foreeach
    foreach($estudiantes  as $indice => $e){
        echo "$e   tiene el indice  $indice <br>";
    }



});


Route::get("countries",function(){

    $countries =[
        "Colombia" => [
           "capital" => "Bogota",
           "moneda" => "peso",
           "poblacion" => 50372424,
           "ciudades" => [
                "Medellin","Cali","Barranquilla"
            ]
        ],
        "Peru" => [

            "capital" => "lima",
           "moneda" => "sol",
           "poblacion" => 33050325,
           "ciudades" =>[
               "cusco","tacna", "arequipa"
           ]
        ] ,
        "Ecuador" => [
            "capital" => "quito",
            "moneda" => "dolar",
            "poblacion" => 17517141,
            "ciudades" =>[
                "cusco","tacna", "arequipa"
            ]
        ],
        "Brazil" => [
            "capital" => "brasilia",
            "moneda" => "real",
            "poblacion" => 	212216052,
            "ciudades" =>[
                "Rio de janeiro","Sao paulo", "Manaos"
            ]
        ],

        "China" => [
        "capital" => "pekin",
        "moneda" => "yuan",
        "poblacion" => 15165165,
        "ciudades" =>[
            "hon kong","Wuhan", "Shanghai"
        ]
    ]];


    //Enviar datos a una vista
    return view('countries')->with("countries" , $countries);
});
