<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body class="bg-white">
    <h1 class="text-center text-primary mt-5 mb-5">Paises del mundo</h1>
    <table class=" container table table-hover  table-bordered  bg-white">
        <thead class="bg-secondary">
            <tr class="text-white">
                <th>Pais</th>
                <th>Capital</th>
                <th>Moneda</th>
                <th>Poblacion</th>
                <td>Ciudades</td>
            </tr>
        </thead>
        <tbody>
            <!-- recorro la tabla foreach blade -->
            @foreach($countries as $p => $infopais)
            <tr >
                <td rowspan="3">{{ $p }}</td>
                <td rowspan="3">{{ $infopais["capital"]    }}</td>
                <td rowspan="3">{{  $infopais["moneda"]    }}</td>
                <td  rowspan="3">{{  $infopais["poblacion"]   }}</td>
                <td> {{  $infopais["ciudades"][0]     }}</td>
            </tr>
            <tr>
               
                    <th>{{ $infopais["ciudades"][1] }}</th>
              
            </tr>
            <tr>
                <th>{{ $infopais["ciudades"][2]   }}</th>
            </tr>
            @endforeach 
        </tbody>

        <div>
        <div>
        <div></div></div></div>
    </table>
        <a href="arreglos">ir a arreglos</a>
</body>
</html>